import React, {Component} from 'react';
import speaker from './speaker.svg';
import './App.css';
import io from 'socket.io-client'

let socket = io(`/`);
socket.on('error', error => {
    console.log('Error: ' + JSON.stringify(error))
});

class App extends Component {
    componentWillMount() {
        this.setState({
            serverStatus: "Disconnected"
        });

        socket.on('connect', () => {
            this.setState({
                serverStatus: "Connected"
            });
            console.log('Connected!')
        });
        socket.on('disconnect', () => {
            this.setState({
                serverStatus: "Disconnected"
            });
            console.log('Disconnected');
        });
        socket.on('nowPlaying', (message) => {
            this.setState({
                nowPlaying: message
            })
        });
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={speaker} id="speaker-icon-left" alt="speaker facing right"/>
                    <h2>RaspPi Sound Board 2.0</h2>
                    <img src={speaker} id="speaker-icon-right" alt="speaker facing left"/>
                    <div id="server-status">Server Status: {this.state.serverStatus}</div>
                </div>

                <FilteredSoundTable />
            </div>
        );
    }
}

class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    handleChange() {
        this.props.onUserInput(
            this.filterTextInput.value
        );
    }

    handleKeyDown(evt) {
        var code = evt.charCode || evt.keyCode;
        if (code === 27) {
            this.filterTextInput.value = '';
            this.handleChange();
        }
    }

    render() {
        return (
            <form>
                <input type="text"
                       placeholder="Search..."
                       value={this.props.filterText}
                       ref={(input) => this.filterTextInput = input}
                       onChange={this.handleChange}
                       onKeyDown={(e) => {
                           this.handleKeyDown(e)
                       } }
                />
            </form>
        );
    }
}

class SoundTable extends Component {
    render() {
        var rows = [];
        this.props.sounds.forEach((sound) => {
            if (sound.shortName.toLowerCase().indexOf(this.props.filterText.toLowerCase()) === -1) {
                return;
            }
            rows.push(<SoundRow sound={sound} key={sound.shortName}/>);
        });
        return (
            <table className="sound-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Added</th>
                    <th>Last Played</th>
                    <th>Play Count</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}

class StopButton extends Component {
    stopSound() {
        socket.emit('stopSound');
    }

    render() {
        return (
            <div id="stop-button" onClick={() => {
                this.stopSound()
            }}>STOP</div>
        )
    }
}

class RandomButton extends Component {
    randomSound() {
        socket.emit('randomSound');
    }

    render() {
        return (
            <div id="random-button" onClick={() => {
                this.randomSound()
            }}>RANDOM</div>
        )
    }
}

class FilteredSoundTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: '',
            sounds: []
        };

        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleSoundsUpdate = this.handleSoundsUpdate.bind(this);
    }

    handleUserInput(filterText) {
        this.setState({
            filterText: filterText
        });
    }

    handleSoundsUpdate(sounds) {
        this.setState({
            sounds: sounds
        })
    }

    componentDidMount() {
        socket.on('sounds', this.handleSoundsUpdate);
    }

    render() {
        return (
            <div id="sound-table-container">
                <RandomButton />
                <SearchBox
                    filterText={this.state.filterText}
                    onUserInput={this.handleUserInput}
                />
                <StopButton />
                <SoundTable sounds={this.state.sounds} filterText={this.state.filterText}/>
            </div>
        );
    }
}

class SoundRow extends Component {
    playSound(fileName) {
        socket.emit('playSound', fileName);
    }

    render() {
        return (
            <tr className="sound-link" onClick={() => {
                this.playSound(this.props.sound.name)
            }}>
                <td>
                    <div className="nowrap">{this.props.sound.shortName}</div>
                </td>
                <td>
                    <div className="nowrap">{this.props.sound.dateAdded}</div>
                </td>
                <td>
                    <div className="nowrap">{this.props.sound.dateLastPlayed}</div>
                </td>
                <td>
                    <div className="nowrap">{this.props.sound.playCount}</div>
                </td>
            </tr>
        );
    }
}

export default App;
