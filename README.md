## Repository for the soundBoard running on Raspberry Pi2 ##
### Uses the player executable:  /usr/bin/mpg321 ###

**Clone to local and run these steps to develop new code:**

1. git-bash at your local TOP level (ie. /codebase/)
1. git clone the repository using link at top right
1. cd into main directory (ie. /codebase/SoundBoardDBPi2/), 'npm install'


### Use Chrome to view client: http://localhost:3000 ###
*Test local changes before pushing to MAC repository.*