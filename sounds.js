var	fs = require('fs');

var soundStats = []; // raw stats file

var soundMap = {}; // a map to allow us to address stats by filename like soundMap[blah]

function readStatsFile() {
	try {
		soundStats = JSON.parse(fs.readFileSync('./sound_stats.json', 'utf8'));
		buildSoundMap();
	} catch (err) {
		console.log('Error reading stats file: ' + err);
	}
}

function buildSoundMap() {
	soundMap = {};
	for (var i = 0; soundStats.length > i; i += 1) {
		soundMap[soundStats[i].filename] = soundStats[i];
	}
}

function writeStatsFile() {
	fs.writeFileSync('./sound_stats.json', JSON.stringify(soundStats));
}

function hasSoundStats(filename) {
	return soundMap[filename];
}

function pad(n) {
    return n<10 ? '0'+n : n;
}

function getDate() {
    var modTime = new Date();
    var dateString = modTime.getFullYear() + '-'
                    + pad(modTime.getMonth()+1) + '-'
                    + pad(modTime.getDate());
	console.log('Date: ' + dateString);
    return dateString;
}

function rebuildStatsFile() {
	var files = fs.readdirSync('./sounds/');
	var newSoundStats = [];
	readStatsFile(); //It is important that we read in the stats file here to ensure we are working with the latest version.

	var newSound = function(filename, dateAdded) {
		return {
			"filename": filename,
			"playCount": 0,
			"dateLastPlayed": "",
            "dateAdded": dateAdded
		}
	};

	files.map(function (filename){
		if (filename.indexOf('.mp3') > 0 || filename.indexOf('.wav') > 0) {
			if (filename != 'Reveille.mp3' && filename != 'Retreat.mp3') {
				if (hasSoundStats(filename)) {
					newSoundStats.push(soundMap[filename]);
				} else {
					console.log('No stats for ' + filename);
					newSoundStats.push(newSound(filename, getDate()));
				}
			}
		}
	});
	soundStats = newSoundStats.slice(); // Replace soundStats with our "fresh" array that accurately reflects the filesystem.
	writeStatsFile();
}

exports.incrementPlayCount = function(filename) {
	soundMap[filename].playCount += 1;
	soundMap[filename].dateLastPlayed = getDate();
	writeStatsFile();
};

exports.getAll = function() {
	var sounds = [];
	rebuildStatsFile();
	soundStats.map(function (soundStat){
		if (soundStat.filename.indexOf('.mp3') > 0 || soundStat.filename.indexOf('.wav') > 0) {
		    if (soundStat.filename != 'Reveille.mp3' && soundStat.filename != 'Retreat.mp3') {
				var lastDot = soundStat.filename.lastIndexOf('.');
				var shortName = soundStat.filename.substring(0, lastDot);
				var sound = { "name":soundStat.filename, "shortName": shortName, "playCount": soundStat.playCount, "dateLastPlayed": soundStat.dateLastPlayed, "dateAdded": soundStat.dateAdded };
				sounds.push(sound);
			}
		}
	});

	return sounds;
};
