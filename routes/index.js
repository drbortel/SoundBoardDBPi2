let express = require('express');
let router = express.Router();
let soundPlayer = require('../soundPlayer');

router.get('/winwinwin', function(req, res, next) {
    soundPlayer.playSound('All I Do Is Win (12s).mp3');
    res.json({"message": "Success"});
});

router.get('/stop', function(req, res, next){
    soundPlayer.stopSounds();
    res.json({"message": "Success"});
});

module.exports = router;