const path = require('path');
const exec = require('child_process').exec;

let playSound = function(fileName) {

    let filePath = path.format({
        dir: path.join(__dirname, 'sounds'),
        base: fileName
    });
    console.log('Trying to play sound: ' + filePath);
    exec('/usr/bin/killall mpg321; /usr/bin/mpg321 ' + '"' + filePath + '"');
};

let stopSounds = function() {
    exec('/usr/bin/killall mpg321');
}

module.exports = {playSound, stopSounds};
