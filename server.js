const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const soundFiles = require('./sounds');
const _ = require('lodash');
const soundPlayer = require('./soundPlayer');

let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let sounds;
let routes = require('./routes/index.js');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

updateSounds();
setInterval(updateSounds, 5000);

app.use('/', routes);

const listeningPort = 3072;

/*TODO: Figure out how to clear the socket (or ignore queued events) when the
 server starts, so when the server dies with browsers still open, users
 mashing buttons will not cause a gazillion things to play as soon as the
 server starts back up.
 */

server.listen(listeningPort, () => {
    console.log(`Find the server at: http://localhost:${listeningPort}/`); // eslint-disable-line no-console
});

function updateSounds() {
    let newSounds = soundFiles.getAll();
    if (newSounds != sounds) {
        sounds = newSounds;
        io.emit('sounds', sounds);
    }
}


io.on('connection', function(socket) {
    // Immediately emit fresh sound array, then send it again every 5 seconds if it changes

    socket.on('stopSound', () => {
        soundPlayer.stopSounds();
        io.emit('nowPlaying', '');
    });
    socket.on('playSound', fileName => {
        try {
            playSound(fileName);
        } catch (err) {
            console.log(err);
            socket.emit('error', {error: err});
        }
    });
        
    socket.on('randomSound', fileName => {
        try {
            randomSound();
        } catch (err) {
            console.log(err);
            socket.emit('error', {error: err});
        }
    });
});

function playSound(fileName) {
    soundFiles.incrementPlayCount(fileName);

    io.emit('nowPlaying', fileName);
    soundPlayer.playSound(fileName);
    io.emit('nowPlaying', '');

    updateSounds();
}

function randomSound() {
    let allSounds = soundFiles.getAll();
    var fileName = allSounds[_.random(allSounds.length-1)].name;
    playSound(fileName);
}
